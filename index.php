<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>UAS CLOUD COMPUTING</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta content="Themesdesign" name="author" />

        <!--favicon-->
        <link rel="shortcut icon" href="images/favicon.ico" />

        <!-- remix icon -->
        <link rel="stylesheet" type="text/css" href="css/remixicon.css">

        <!-- css -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/style.min.css" rel="stylesheet" type="text/css" />
        <link href="css/animate.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <!--navbar start-->
        <nav class="navbar navbar-expand-lg fixed-top navbar-light sticky">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="images/logo.png" alt="" class="logo-light" height="100" />
                    <img src="images/logo.png" alt="" class="logo-dark" height="20" />
                </a>
                <a href="javascript:void(0);" class="navbar-toggler ml-auto" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggle-icon"><i class="ri-menu-line"></i></span>
                </a>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#tabel">Tabel</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--navbar end-->

        <!--hero start-->
        <section class="hero-2 overflow-hidden position-relative" id="home">
            <div class="bg-text d-none d-xl-block">
                <h1 class="font-weight-bold hero-bg-text">
                    I'm <br />
                    Jericho <br />
                    Alfa
                </h1>
            </div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <h5 class="font-weight-normal text-muted mt-2">Hi there,</h5>
                        <h1 class="text-dark mb-4">I'm Jericho Alfa</h1>
                        <p class="text-muted mb-4">
                            16041808 - Web ini digunakan dalam UAS Cloud Computing. Pada bagian tabel, akan berisikan isi dari database.
                        </p>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <img class="rounded hero-img mt-lg-0 mt-5" src="images/3dchar.png" alt="" />
                    </div>
                </div>
                <!-- end row -->
            </div>
        </section>
        <!--hero end-->

        <!--tabel start-->
        <section class="section" id="tabel">
            <div class="container">
                <div class="row">
                    <div class="col-12 mb-4">                        
                        <h2 class="title">Tabel<span class="title-border ml-4 pl-3"></span></h2>                        
                    </div>
                </div>
                <!-- end row -->
                <div class="row">
                <?php
                include 'koneksi.php';
                $conn = OpenCon();
                echo "Connected Successfully";
                CloseCon($conn);
                 /*$sql = "SELECT * from biodata";
                $result = $mysqli->query($sql);
                $row = $result->fetch_assoc();


                
                $conn = OpenCon();
                echo "Connected Successfully";
                CloseCon($conn);
                 $servername = "localhost";
                 $username = "root";
                 $password = "three";
                 $dbname = "jericho";

                 $conn = new mysqli($servername,$username,$password,$dbname);
                 if($conn->connect_error){
                     die("Connection Failed: " . $conn->connect_error);
                 }
                 $sql = "SELECT * from biodata";
                 $result = $conn->query($sql);  

                 if($result->num_rows > 0){
                     while($row = $result -> fetch_assoc()){
                         echo "id: " . $row["id"]. " - Name: " . $row['nama'];
                     }
                 }*/
                ?>
                    <div class="col-12 offset-md-1 mt-4 pt-2">
                        <table style="width:100%">
                            <tr>
                                <th>ID</th>
                                <th>NRP</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Kota</th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>160418108</td>
                                <td>Jericho</td>
                                <td>Tenggilis</td>
                                <td>Surabaya</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- end row -->
            </div>
        </section>
        <!-- about us end -->

        

        
        <!-- footer start -->
        <footer class="footer" style="background-image: url(images/footer-bg.jpg);">
            <div class="container">
                <div class="row py-sm-5 py-3">
                    <div class="col-lg-8 col-md-2 text-white mt-5">
                        <h2 class="mb-0 title text-white">Get in touch </h2>
                        <p class="mt-0">For project inquiry please send email to <a href="mailto:4djericho@gmail.com" class="text-white">4djericho@gmail.com</a></p>
                        <ul class="list-unstyled">
                            <li class="list-inline d-inline-block social-list">
                                <a class="list-inline-item rounded mr-2 social-link" href="#"><i class="ri-facebook-fill"></i></a>
                            </li>
                            <li class="list-inline d-inline-block social-list">
                                <a class="list-inline-item rounded mr-2 social-link" href="#"><i class="ri-instagram-fill"></i></a>
                            </li>
                            <li class="list-inline d-inline-block social-list">
                                <a class="list-inline-item rounded mr-2 social-link" href="#"><i class="ri-dribbble-fill"></i></a>
                            </li>
                            <li class="list-inline d-inline-block social-list">
                                <a class="list-inline-item rounded mr-2 social-link" href="#"><i class="ri-linkedin-fill"></i></a>
                            </li>
                            <li class="list-inline d-inline-block social-list">
                                <a class="list-inline-item rounded mr-2 social-link" href="#"><i class="ri-github-fill"></i></a>
                            </li>
                        </ul>
                    </div>
                    
                </div>
                <!-- end row -->
            </div>
        </footer>
        <!-- end footer -->


        <script src="js/jquery-3.5.1.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>

        <!-- smooth link -->
        <script src="js/scrollspy.min.js"></script>
        <script src="js/jquery.easing.min.js"></script>

        <!-- portfolio filter -->
        <script src="js/isotope.js"></script>
        <script src="js/portfolio.init.js"></script>

        <script src="js/app.js"></script>

        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript" src="js/wow.min.js"></script>


    </body>
</html>
